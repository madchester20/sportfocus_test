var path = require('path'),
	webpack = require('webpack'),
	buildPath = path.resolve(__dirname, 'public', 'build');

module.exports = {
	  entry: './app/index.js',
	  output: { path: buildPath, filename: 'bundle.js' },
	  module: {
	    loaders: [
	      {
	        test: /.jsx?$/,
	        loader: 'babel',
	        exclude: /node_modules/,
	        query: {
	        	plugins: ['transform-decorators-legacy' ],
    			presets: ['es2015', 'stage-0', 'react']
	        },
	      }, 
	      { test: /\.json$/, loader: 'json' },
	    ]
	  },
	  plugins:[new webpack.HotModuleReplacementPlugin()]

};
