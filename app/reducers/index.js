import {combineReducers} from 'redux';
import {GET_ALL_IMAGES, GET_IMAGES} from '../actions/actionTypes';
const INITIAL_STATE = { list_events:null };

function getImages (state = [], action) {
  console.log('action', action.type);
  switch (action.type) {
    case 'GET_ALL_IMAGES':
      return action.payload
      return Object.assign({}, state, {
				list_events: action.payload
			});
    case 'GET_IMAGES':
    	return action.payload
    default:
      return state;
  }
}
const reducers = combineReducers({getImages: getImages})

export default reducers