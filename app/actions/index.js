import axios from 'axios';
import * as types from './actionTypes';
import sample from './images.json';
import _ from 'underscore';
export function getAllImages(){

	//return dispatch =>{
		let request = axios.get('http://development.sportsinfocus.com.au/api/1.0/events/current');
		//console.log('GET ALL IMAGES',request);
		// request.then((data) =>{
		// 	console.log('DATA', data);
		// 	const {request} = data
		// 	return {
		// 		type: types.GET_ALL_IMAGES,
		// 		payload: data.data
		// 	};
		// })
		return {
			type: types.GET_ALL_IMAGES,
			payload: request
		}
	//}
}

export function getImages(params){
	//console.log('PARAMS', sample.Items);
	let list = sample.Items
	let result = _.find(sample.Items, function(data){ return data.slug === params; });
	// let result = list.map((data)=>{
	// 	console.log('DATA', data);
	// })
	//console.log('GET IMAGES', result);
	return {
		type: types.GET_IMAGES,
		payload: result
	}
}
