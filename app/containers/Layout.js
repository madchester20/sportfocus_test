import React, {Component} from 'react';
import {Button, NavItem, NavDropdown, MenuItem, Nav, Navbar} from 'react-bootstrap';
import {getAllImages, getImages} from '../actions/index';

import Sports from '../components/Sports';
import SportsImages from '../components/SportsImages';

import {bindActionCreators} from 'redux';
import { connect } from "react-redux";

class Layout extends Component{
  constructor(props) {
    super(props);
    this.state = {
      images:"",
      selectedSports: null
    }
    this.handleActions = this.handleActions.bind(this);
    this.handleSelectSports = this.handleSelectSports.bind(this);
  }
  componentDidMount() {
    this.props.getAllImages();
  }
  componentWillReceiveProps(nextProps){
    console.log('NEXTPROPS',nextProps);
  } 
  handleActions(){
    this.props.getAllImages();
    this.setState({images:"get"})
  }
  handleSelectSports(data,e,term) {
    //this.context.router.push(`/event/${data}`);
    console.log('TERM', data);
    
    this.setState({selectedSports: data})
  }
  render() {
    let list = this.props
    const {selectedSports} = this.state
    let events = this.props.images &&  this.props.images.getImages.data ? this.props.images.getImages.data.data.data : null
    //console.log('LIST',list);
    //console.log('LIST',events);
    return (
      <div className='container'>
        <div className='container-fluid'>
          <Navbar>
            <Navbar.Header>
              <Navbar.Brand>
                <a href="#">Sport Focus</a>
              </Navbar.Brand>
            </Navbar.Header>
            <Nav>
            </Nav>
          </Navbar>
          {
            selectedSports ? <SportsImages selectedSports={selectedSports} />
            : <Sports handleSelectSports={this.handleSelectSports} events={events} />
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {images: state};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({getAllImages, getImages}, dispatch)
  console.log('bind', bindActionCreators);
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);