import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Button,Grid, Row, Col, Thumbnail, NavItem, NavDropdown, MenuItem, Nav, Navbar} from 'react-bootstrap';

import {getImages} from '../actions/index';

import {bindActionCreators} from 'redux';
import { connect } from "react-redux";

import Viewer from 'react-viewer';
//import './assets/css/viewer.css';

class SportsImages extends Component{
	constructor(props){
		super(props)
		this.state = {
			visible: false
		}
		this.handleViewImage = this.handleViewImage.bind(this);
	}
	componentDidMount() {
		const {selectedSports} = this.props
		this.props.getImages(selectedSports);
	}
	handleViewImage(){
		this.setState({visible: true})
	}
	render(){	
		const {selectedSports, images} = this.props
		//console.log('SPORTS IMAGES', this.props);
		let source = images.getImages.s3_key
		let alt = images.getImages.slug
		let image =[
			{
				src: source,
				alt: alt
			}
		]
		return(
			<div>
		 	<h2 className="text-center">{selectedSports}</h2>
			<Grid>
		    <Row>
		    <Col xs={6} md={4}>
		      <Thumbnail src={images.getImages.s3_key} alt="242x200">
		        <h3>Thumbnail label</h3>
		        <p>Region : {images.getImages.s3_region}</p>
		        <p>Picture ID : {images.getImages.id}</p>
		        <p>
		          <Button onClick = {this.handleViewImage}bsStyle="primary">View</Button>&nbsp;
		           <Viewer
					        visible={this.state.visible}
					        onClose={() => { this.setState({ visible: false }); } }
					        images={image}
				        />
		        </p>
		      </Thumbnail>
		    </Col>
		    </Row>
		  </Grid>
		  </div>
			)
	}
}
function mapStateToProps(state) {
  return {images: state};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({getImages}, dispatch)
  console.log('bind', bindActionCreators);
}

export default connect(mapStateToProps, mapDispatchToProps)(SportsImages);