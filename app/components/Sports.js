import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Button, NavItem, NavDropdown, MenuItem, Nav, Navbar, Accordion, Panel} from 'react-bootstrap';
import SportsLists from './SportsLists';
class Sports extends Component{
	constructor(props){
		super(props)
	}
	componentDidMount() {
	}
	render(){
		const {events, handleSelectSports} = this.props
		//console.log('SPORTS',events);
		let total_events = events ? events.length : null;
		const listofevents = events ? events.map((data,index)=>{
				//console.log('DATA', data);
				return <SportsLists handleSelectSports={handleSelectSports} sports={data} key = {index}/>
			})
		: null
		return(
			<div className="container-fluid">
		    {listofevents}
		  </div>
			)
	}
}

export default Sports;