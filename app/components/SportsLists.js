import React from 'react';
import ReactDOM from 'react-dom';
import {Button, Col, FormControl, NavItem, NavDropdown, MenuItem, Nav, Navbar, Panel,FormGroup, ControlLabel} from 'react-bootstrap';


const SportList = (props) =>{
	const {index, sports,total_events, handleSelectSports} = props
	//console.log('SPORT LIST', sports.slug);
	return(
			<Panel header={sports.name} bsStyle="primary" onClick={handleSelectSports.bind(this,sports.slug)}>
	      <FormGroup>
		      <ControlLabel>Manager</ControlLabel>
		      <FormControl.Static>
		        {sports.manager}
		      </FormControl.Static>
		      <ControlLabel>Sport</ControlLabel>
		      <FormControl.Static>
		        {sports.sport}
		      </FormControl.Static>
		    </FormGroup>
	    </Panel>

		)
}


export default SportList
