// import React from 'react';
// import ReactDOM from 'react-dom';
// import Layout from './containers/Layout'

// ReactDOM.render(<Layout/>, document.getElementById('app'));

import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './containers/Layout'

// import logger from "redux-logger";
// import thunk from "redux-thunk";
import promise from "redux-promise-middleware";
import ReduxPromise from 'redux-promise';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux';

import reducers from './reducers/index'

const middleware = applyMiddleware(ReduxPromise, thunk, logger());;
const store = createStore(reducers, middleware);
//const store = createStoreWithMiddleware(reducers);
//console.log('store', store);
//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<Provider store={store}><Layout /></Provider>, document.getElementById('app'));